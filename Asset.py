from sortedcontainers import SortedKeyList

from Log import Log
from ValueFunction import *


class Asset:
    """
    An asset is a body of wealth. It has various costs and profits associated with it (abstractly represented as
    ValueFunctions).
    """

    def __init__(self, name: str, initialValue: float = 0.0, creationTime: float = 0.0):
        self.name = name
        self.valueFunctions: SortedKeyList[ValueFunction] = SortedKeyList(key=getValueFunctionNextEvaluationTime)
        self.value: float = initialValue
        self.lastEvaluationTime = creationTime
        self.log: Dict[float, float] = {creationTime: initialValue}

    def addValueFunction(self, profit: ValueFunction):
        self.valueFunctions.add(profit)

    def getNextProfitEvaluationTime(self) -> float:
        if len(self.valueFunctions) == 0:
            return float("inf")
        return self.valueFunctions[0].nextEvaluationTime

    def getNextEvaluationTime(self) -> float:
        if len(self.valueFunctions) == 0:
            return float("inf")
        return self.valueFunctions[0].nextEvaluationTime

    def getLog(self) -> Log:
        return Log(self.log)

    def update(self, assetsState: AssetsState):
        """
        Apply the next profit or cost.
        """

        def updateAndApplyValueFunction(asset: Asset, valueFunction: ValueFunction):
            """Helper function for updating and applying value functions."""
            # Determine whether the value function is fixed or percentage.
            if issubclass(type(valueFunction), FixedValueFunction):
                fixedValueFunction: FixedValueFunction = valueFunction
                asset.value += fixedValueFunction.getValueAndUpdateNextEvaluationTime()
            elif issubclass(type(valueFunction), PercentageValueFunction):
                percentageValueFunction: PercentageValueFunction = valueFunction
                asset.value += percentageValueFunction.getValueAndUpdateNextEvaluationTime(asset.value)
            elif issubclass(type(valueFunction), CalculatedValueFunction):
                calculatedValueFunction: CalculatedValueFunction = valueFunction
                asset.value += calculatedValueFunction.getValueAndUpdateNextEvaluationTime(assetsState)
            else:
                raise RuntimeWarning("Applying ValueFunction of type {} has not been implemented in {}".format(str(type(valueFunction)), __name__))

        # Check whether the next value function is a profit or a cost and then apply it.
        if self.getNextEvaluationTime() == float("inf"):
            raise RuntimeWarning("The next evaluation time for Asset {} was infinite which shouldn't be called.".format(self.name))
        else:
            # Update the last evaluation time as the next evaluation time.
            self.lastEvaluationTime = self.getNextEvaluationTime()
            # Evaluate the next ValueFunction.
            nextValueFunction = self.valueFunctions.pop(0)
            updateAndApplyValueFunction(self, nextValueFunction)
            # Add the value function that was evaluated back into the valueFunctions list.
            self.valueFunctions.add(nextValueFunction)
            # Log the asset value.
            self.log[self.lastEvaluationTime] = self.value

    def transactionUpdate(self, transactionValue: float, transactionTime: float):
        """Applies a transaction value update."""
        # Update the last evaluation time to the transaction time.
        self.lastEvaluationTime = transactionTime
        # Add the transaction value.
        self.value += transactionValue
        # Log the asset value.
        self.log[self.lastEvaluationTime] = self.value


def getAssetNextEvaluationTime(asset: Asset) -> float:
    """
    Takes an Asset and returns the next time that this Asset is supposed to be evaluated. If it isn't supposed to be
    evaluated it returns infinite.

    :param asset: The Asset to get the time from.
    :return: A float representing the next time in seconds that the Asset is supposed to be evaluated.
    """
    nextEvaluationTime = asset.getNextEvaluationTime()
    if nextEvaluationTime is None:
        return float("inf")
    return nextEvaluationTime
