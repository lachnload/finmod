import csv
import os
from collections import OrderedDict
from typing import Dict, List

import numpy as np


from Constants import INFINITE


class Log:
    """A log is a time series of data."""

    def __init__(self, data: Dict[float, float]):
        # Save the raw form of the data guaranteed as sorted.
        self.dataDict = OrderedDict([(time, data[time]) for time in sorted(data.keys())])
        # Provide the sorted time series as an array for convenience as well.
        self.dataArray = np.array([[time, dataPoint] for time, dataPoint in self.dataDict.items()])


def sumLogs(log1: Log, log2: Log) -> Log:
    """
    Sums two logs together to generate a new log. The logs are assumed to contain piecewise, step data with same or
    different timestamps. The sum is defined as the superposition of these two functions.
    """
    # Get the value changes in both logs.
    deltas1 = np.diff(log1.dataArray[:, 1])
    deltas2 = np.diff(log2.dataArray[:, 1])

    # Add the initial value as the first value change.
    deltas1 = np.insert(deltas1, 0, log1.dataArray[0, 1])
    deltas2 = np.insert(deltas2, 0, log2.dataArray[0, 1])

    # Iterate over both deltas adding them together.
    summ = 0.0
    summedData = {}
    i1 = 0
    i2 = 0
    time1 = log1.dataArray[i1][0]
    time2 = log2.dataArray[i2][0]
    while i1 < len(deltas1) or i2 < len(deltas2):
        # Update summ and build summedData
        if time1 == time2:
            summ += deltas1[i1] + deltas2[i2]
            summedData[time1] = summ
            i1 += 1
            i2 += 1
        elif time1 < time2:
            summ += deltas1[i1]
            summedData[time1] = summ
            i1 += 1
        else:  # time1 > time2
            summ += deltas2[i2]
            summedData[time2] = summ
            i2 += 1
        # Update the times. Ensure that the times are set to infinite when we've iterated over the whole log.
        if i1 < len(deltas1):
            time1 = log1.dataArray[i1][0]
        else:
            time1 = INFINITE
        if i2 < len(deltas2):
            time2 = log2.dataArray[i2][0]
        else:
            time2 = INFINITE
    # Return the summed log.
    return Log(summedData)


def sumListLogs(logs: List[Log]) -> Log:
    """Sums a list of logs together recursively."""
    if len(logs) == 0:
        return Log({})
    if len(logs) == 1:
        return logs[0]
    return sumLogs(sumListLogs(logs[:-1]), logs[-1])


def saveLog(folderpath: str, filename: str, log: Log):
    """Saves a log to the given folderpath with the given filename as a csv."""
    with open(os.path.join(folderpath, filename + ".csv"), "w") as file:
        for (time, value) in log.dataDict.items():
            file.write("{},{}\n".format(time, value))


def loadLog(filepath: str) -> Log:
    """Loads a log from the given filepath."""
    with open(filepath, "r") as file:
        csvReader = csv.reader(file)
        return Log({float(row[0]): float(row[1]) for row in csvReader})
