from pathlib import Path
from typing import Dict, List
from sortedcontainers import SortedKeyList

from TerminationCondition import TerminationCondition
from Asset import Asset, getAssetNextEvaluationTime
from Log import Log, saveLog
from Transaction import Transaction, getTransactionNextEvaluationTime
from Constants import *
from ValueFunction import AssetsState


class System:
    """A System is a set of Assets that potentially have Transactions coupling them that can be simulated."""

    def __init__(self, name: str):
        self.name = name
        self.assets: SortedKeyList[Asset] = SortedKeyList(key=getAssetNextEvaluationTime)
        self.transactions: SortedKeyList[Transaction] = SortedKeyList(key=getTransactionNextEvaluationTime)
        self.terminationConditions: List[TerminationCondition] = []
        self.lastEvaluationTime = -float("inf")

    def addAsset(self, asset: Asset):
        self.assets.add(asset)

    def addTransaction(self, transaction: Transaction):
        self.transactions.add(transaction)

    def addTerminationCondition(self, terminationCondition: TerminationCondition):
        self.terminationConditions.append(terminationCondition)

    def getNextAssetEvaluationTime(self) -> float:
        if len(self.assets) == 0:
            return float("inf")
        return self.assets[0].getNextEvaluationTime()

    def getNextTransactionEvaluationTime(self) -> float:
        if len(self.transactions) == 0:
            return float("inf")
        return self.transactions[0].getNextEvaluationTime()

    def getNextEvaluationTime(self) -> float:
        if self.getNextAssetEvaluationTime() < self.getNextTransactionEvaluationTime():
            return self.getNextAssetEvaluationTime()
        else:
            return self.getNextTransactionEvaluationTime()

    def getAssetsState(self) -> AssetsState:
        assetsState: Dict[str, float] = {}
        for asset in self.assets:
            assetsState[asset.name] = asset.value
        return assetsState

    def simulate(self, terminationTime: float):
        """
        Simulates the system until the terminationTime is reached, a terminationCondition is reached or there is nothing
        else to update.
        """

        def update(self: System):
            """
            Update the next Asset or apply the next Transaction.
            """
            # Determine whether it is an Asset or Transaction to be handled next.
            if self.getNextEvaluationTime() == INFINITE:
                return
            elif self.getNextAssetEvaluationTime() < self.getNextTransactionEvaluationTime():
                assetsState = self.getAssetsState()
                asset = self.assets.pop(0)
                asset.update(assetsState)
                self.addAsset(asset)
            else:
                transaction = self.transactions.pop(0)
                transaction.update(self.getAssetsState())
                self.addTransaction(transaction)

        # Keep updating the system until the termination time is reached or there is nothing else to evaluate.
        nextEvaluationTime = self.getNextEvaluationTime()
        while nextEvaluationTime <= terminationTime and nextEvaluationTime != INFINITE:
            # Update the system.
            update(self)
            # Update the time.
            self.lastEvaluationTime = nextEvaluationTime
            nextEvaluationTime = self.getNextEvaluationTime()
            # Check whether a termination condition has been satisfied.
            for terminationCondition in self.terminationConditions:
                if terminationCondition.check():
                    return


def saveSystemSimulationLogs(folderpath: str, system: System):
    """Save the system simulation log to the given folderpath."""
    # Make the folderpath if it doesn't already exist.
    Path(folderpath).mkdir(parents=True, exist_ok=True)
    # Save all the assets and their value functions.
    for asset in system.assets:
        # Save the asset.
        assetFilename = "asset_" + asset.name
        saveLog(folderpath, assetFilename, asset.getLog())
        # Save all the value functions of this asset.
        for valueFunction in asset.valueFunctions:
            # Save the value function.
            saveLog(folderpath, assetFilename + "-valueFunction_" + valueFunction.name, valueFunction.getLog())
    # Save all the transactions and their value functions.
    for transaction in system.transactions:
        # Save the transaction.
        transactionFilename = "transaction_" + transaction.name
        saveLog(folderpath, transactionFilename, transaction.getLog())
        # Save the value function.
        saveLog(folderpath, transactionFilename + "-valueFunction_" + transaction.valueFunction.name,
                transaction.valueFunction.getLog())
