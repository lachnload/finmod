from Asset import Asset


class TerminationCondition:
    """
    Describes a condition under which a simulation should be terminated.
    """

    def check(self) -> bool:
        """
        Checks whether the termination condition has been evaluated.
        """
        pass


class AssetValueGreaterThanTerminationCondition(TerminationCondition):
    """
    Describes a TerminationCondition due to an asset value becoming greater than a value.
    """

    def __init__(self, asset: Asset, value: float):
        self.asset = asset
        self.value = value

    def check(self) -> bool:
        return self.asset.value > self.value


class AssetValueLessThanTerminationCondition(TerminationCondition):
    """
    Describes a TerminationCondition due to an asset value becoming less than a value.
    """

    def __init__(self, asset: Asset, value: float):
        self.asset = asset
        self.value = value

    def check(self) -> bool:
        return self.asset.value < self.value
