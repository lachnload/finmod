# Unit test foldername.
UNIT_TEST_FOLDERNAME = "unittests"

# Infinite.
INFINITE = float("inf")

# Time conversion constants.
SECONDS_IN_DAY = 86400
SECONDS_IN_WEEK = SECONDS_IN_DAY * 7
SECONDS_IN_MONTH = SECONDS_IN_WEEK * 4
SECONDS_IN_YEAR = SECONDS_IN_MONTH * 12
