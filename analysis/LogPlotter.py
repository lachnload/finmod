import argparse
import os
from collections import OrderedDict
from pathlib import Path
from typing import List
import matplotlib.pyplot as plt

from Log import loadLog, sumListLogs
from Constants import SECONDS_IN_YEAR


def plotLogs(logs: OrderedDict, logsToPlot: List[str]):
    """Plots the selected logs."""
    # Plot each log that is to be plotted.
    for logName in logsToPlot:
        plt.plot(logs[logName].dataArray[:, 0]/SECONDS_IN_YEAR, logs[logName].dataArray[:, 1], label=logName)
    plt.ylabel("Value ($)")
    plt.xlabel("Time (yr)")
    plt.legend()
    plt.ion()
    plt.show(block=True)


def loadLogs(logFolderpath: str) -> OrderedDict:
    """Loads the logs contained in the provided folderpath."""
    # Check the given folderpath is valid.
    if not os.path.isdir(logFolderpath):
        raise RuntimeError("The log folderpath given doesn't exist. Given folderpath: {}".format(logFolderpath))
    # Iterate through all the csv files in the folder.
    csvFilepaths = Path(logFolderpath).rglob("*.csv")
    logs = OrderedDict()
    for path in csvFilepaths:
        logs[os.path.basename(os.path.splitext(path)[0])] = loadLog(str(path))
    # Return the logs.
    return logs


def runLogPlotter(logFolderpath: str):
    """
    Runs a plotter for logs. This is an interactive CLI that allows the user to generate plots from the data provided
    in the log folderpath.
    """
    # Process the log folderpath to get all the logs out.
    logs = loadLogs(logFolderpath)
    enumeratedLogNames = {i: logName for i, logName in enumerate(logs.keys())}

    def selectLogNamesToPlot() -> List[str]:
        print("Please select the log(s) you'd like plotted. If you want to plot multiple logs separate them with commas.")
        for i, logName in enumeratedLogNames.items():
            print("[{}]: {}".format(i, logName))
        logSelection = input("Enter comma separated log numbers: ")
        try:
            return [enumeratedLogNames[float(i.strip())] for i in logSelection.split(",")]
        except ValueError as ve:
            print("Your input was invalid. Perhaps you didn't have comma separated numbers.")
            print()
            print("ValueError")
            print(str(ve))
            return []

    while True:
        print()
        print()
        print("Please select from the following options.")
        print("[1]: Plot log(s).")
        print("[2]: Plot logs summed together.")
        print("[0]: Exit.")
        command = input("Enter option as a single digit: ")
        print()
        if command == "1":
            # Get the logs that have been selected.
            logNamesToPlot = selectLogNamesToPlot()
            # Plot and display them.
            plotLogs(logs, logNamesToPlot)
        elif command == "2":
            # Get the logs that have been selected.
            logNamesToPlot = selectLogNamesToPlot()
            # Add them together.
            summedLog = sumListLogs([logs[logName] for logName in logNamesToPlot])
            # Create a name for the logs.
            summedLogName = "\n".join(logNamesToPlot)
            # Plot and display them.
            plotLogs(OrderedDict([(summedLogName, summedLog)]), [summedLogName])
        elif command == "0":
            break
        else:
            print("Your input '{}' was not a valid option.".format(command))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--logFolderpath", help="The folderpath containing the logs we'll be plotting.",
                        required=True, type=str)
    args = parser.parse_args()

    runLogPlotter(args.logFolderpath)
