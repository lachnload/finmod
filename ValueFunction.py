from collections import OrderedDict
from typing import Dict
from simpleeval import simple_eval, NameNotDefined

from Constants import *
from Log import Log


AssetsState = Dict[str, float]


class ValueFunction:
    """
    Describes how value changes over time.
    """

    def __init__(self, name: str, firstEvaluationTime: float):
        self.name = name
        self.nextEvaluationTime: float = firstEvaluationTime
        self.log: Dict[float, float] = {}

    def getLog(self) -> Log:
        return Log(self.log)


class FixedValueFunction(ValueFunction):
    """
    A ValueFunction that returns a value when asked without requiring additional inputs.
    """

    def __init__(self, name: str, firstEvaluationTime: float):
        ValueFunction.__init__(self, name, firstEvaluationTime)

    def getValueAndUpdateNextEvaluationTime(self) -> float:
        pass


class PercentageValueFunction(ValueFunction):
    """
    A ValueFunction that returns a value that is the percentage of a baseValue.
    """

    def __init__(self, name: str, firstEvaluationTime: float):
        ValueFunction.__init__(self, name, firstEvaluationTime)

    def getValueAndUpdateNextEvaluationTime(self, baseValue: float) -> float:
        pass


class CalculatedValueFunction(ValueFunction):
    """
    A ValueFunction that returns a value that is calculated from the values of assets.
    """

    def __init__(self, name: str, firstEvaluationTime: float):
        ValueFunction.__init__(self, name, firstEvaluationTime)

    def getValueAndUpdateNextEvaluationTime(self, values: AssetsState) -> float:
        pass


class SingleUseFixedValueFunction(FixedValueFunction):
    """
    A value function that is applied only once with a fixed value.
    """

    def __init__(self, name: str, evaluationTime: float, value: float):
        FixedValueFunction.__init__(self, name, evaluationTime)
        self.value = value
        self.evaluated = False

    def getValueAndUpdateNextEvaluationTime(self) -> float:
        # Log the value.
        self.log[self.nextEvaluationTime] = self.value
        # Indicate there are no more updates to be performed.
        self.nextEvaluationTime = INFINITE
        # Return value.
        return self.value


class SingleUsePercentageValueFunction(PercentageValueFunction):
    """
    A value function that is applied only once with a percentage value.
    """

    def __init__(self, name: str, evaluationTime: float, percentage: float):
        PercentageValueFunction.__init__(self, name, evaluationTime)
        self.percentage = percentage
        self.evaluated = False

    def getValueAndUpdateNextEvaluationTime(self, baseValue: float) -> float:
        # Calculate the total value to be returned.
        value = self.percentage * baseValue
        # Log the value.
        self.log[self.nextEvaluationTime] = value
        # Indicate there are no more updates to be performed.
        self.nextEvaluationTime = INFINITE
        # Return value.
        return value


class SingleUseCalculatedValueFunction(CalculatedValueFunction):
    """
    A value function that is applied only once with a calculated value.
    """

    def __init__(self, name: str, evaluationTime: float, formula: str):
        CalculatedValueFunction.__init__(self, name, evaluationTime)
        self.formula = formula
        self.evaluated = False

    def getValueAndUpdateNextEvaluationTime(self, values: AssetsState) -> float:
        # Update all the values in the formula.
        thisFormula = self.formula
        for key, value in values.items():
            thisFormula = thisFormula.replace(key, str(value))
        # Evaluate the formula and return the result.
        try:
            # Calculate the value to be returned.
            value = simple_eval(thisFormula)
            # Log the value.
            self.log[self.nextEvaluationTime] = value
            # Indicate there are no more updates to be performed.
            self.nextEvaluationTime = INFINITE
            # Return value.
            return value
        # Handle the formula not referencing a real asset.
        except NameNotDefined as ex:
            raise NameNotDefined(f"The evaluation of the SingleUseCalculatedValueFunction named {self.name} failed.\n"
                                 f"{ex.message}", ex.expression)


class RecurringValueFunction(ValueFunction):
    """
    A value function that is applied many times.
    """

    def __init__(self, name: str, firstEvaluationTime: float, evaluationPeriod: float, terminationTime: float = INFINITE):
        ValueFunction.__init__(self, name, firstEvaluationTime)
        self.evaluationPeriod = evaluationPeriod
        self.terminationTime = terminationTime

    def updateNextEvaluationTime(self):
        self.nextEvaluationTime += self.evaluationPeriod
        if self.nextEvaluationTime > self.terminationTime:
            self.nextEvaluationTime = INFINITE


class RecurringFixedValueFunction(RecurringValueFunction, FixedValueFunction):
    """
    A value function that is applied many times with a fixed value every time.
    """

    def __init__(self, name: str, firstEvaluationTime: float, evaluationPeriod: float, value: float, terminationTime: float = INFINITE):
        RecurringValueFunction.__init__(self, name, firstEvaluationTime, evaluationPeriod, terminationTime)
        self.value = value

    def getValueAndUpdateNextEvaluationTime(self) -> float:
        # Log the value.
        self.log[self.nextEvaluationTime] = self.value
        # Calculate the next evaluation time.
        self.updateNextEvaluationTime()
        # Return value.
        return self.value


class RecurringPercentageValueFunction(RecurringValueFunction, PercentageValueFunction):
    """
    A value function that is applied many times with a percentage value every time.
    """

    def __init__(self, name: str, firstEvaluationTime: float, evaluationPeriod: float, percentage: float, terminationTime: float = INFINITE):
        """
        :param percentage: The percentage as a decimal.
        """
        RecurringValueFunction.__init__(self, name, firstEvaluationTime, evaluationPeriod, terminationTime)
        self.percentage = percentage

    def getValueAndUpdateNextEvaluationTime(self, baseValue: float) -> float:
        # Calculate the total value to be returned.
        value = self.percentage * baseValue
        # Log the value.
        self.log[self.nextEvaluationTime] = value
        # Update the next evaluation time.
        self.updateNextEvaluationTime()
        # Return value.
        return value


class RecurringCalculatedValueFunction(RecurringValueFunction, CalculatedValueFunction):
    """
    A value function that is applied only once with a calculated value.
    """

    def __init__(self, name: str, firstEvaluationTime: float, evaluationPeriod: float, formula: str, terminationTime: float = INFINITE):
        RecurringValueFunction.__init__(self, name, firstEvaluationTime, evaluationPeriod, terminationTime)
        self.formula = formula

    def getValueAndUpdateNextEvaluationTime(self, values: AssetsState) -> float:
        # Update all the values in the formula.
        thisFormula = self.formula
        for key, value in values.items():
            thisFormula = thisFormula.replace(key, str(value))
        # Evaluate the formula and return the result.
        try:
            # Calculate the value to be returned.
            value = simple_eval(thisFormula)
            # Log the value.
            self.log[self.nextEvaluationTime] = value
            # Update the next evaluation time.
            self.updateNextEvaluationTime()
            # Return value.
            return value
        # Handle the formula not referencing a real asset.
        except NameNotDefined as ex:
            raise NameNotDefined(f"The evaluation of the RecurringCalculatedValueFunction named {self.name} failed.\n"
                                 f"{ex.message}", ex.expression)


class FixedValueFunctionProfile(FixedValueFunction):
    """
    A profile of fixed values with each value applied at a given time.
    """

    def __init__(self, name: str, fixedValueProfile: Dict[float, float]):
        # Sort the profile.
        self.fixedValueProfile = OrderedDict(sorted(fixedValueProfile.items(), key=lambda kv: kv[0]))
        # Set the next evaluation time.
        FixedValueFunction.__init__(self, name, list(self.fixedValueProfile.keys())[0])

    def getValueAndUpdateNextEvaluationTime(self) -> float:
        # Pop the first item from the profile.
        _, value = self.fixedValueProfile.pop(0)
        # Log the value.
        self.log[self.nextEvaluationTime] = value
        # Update the next evaluation time.
        self.nextEvaluationTime = list(self.fixedValueProfile.keys())[0]
        # Return value.
        return value


class PercentageValueFunctionProfile(PercentageValueFunction):
    """
    A profile of value percentages with each percentage applied at a given time.
    """

    def __init__(self, name: str, percentageProfile: Dict[float, float]):
        """
        :param percentageProfile: An ordered dictionary of times and percentages with percentages given in decimal.
        """
        # Sort the profile.
        self.percentageProfile = OrderedDict(sorted(percentageProfile.items(), key=lambda kv: kv[0]))
        # Set the next evaluation time.
        PercentageValueFunction.__init__(self, name, list(self.percentageProfile.keys())[0])

    def getValueAndUpdateNextEvaluationTime(self, baseValue: float) -> float:
        # Pop the first item from the profile.
        _, percentage = self.percentageProfile.pop(0)
        # Calculate the total value to be returned.
        value = baseValue * percentage
        # Log the value.
        self.log[self.nextEvaluationTime] = value
        # Update the next evaluation time.
        self.nextEvaluationTime = list(self.percentageProfile.keys())[0]
        # Return value.
        return value


def getValueFunctionNextEvaluationTime(valueFunction: ValueFunction) -> float:
    """
    Takes a ValueFunction and returns the next time that this value function is supposed to be evaluated. If it isn't
    supposed to be evaluated it returns infinite.

    :param valueFunction: The ValueFunction to get the time from.
    :return: A float representing the next time in seconds that the ValueFunction is supposed to be evaluated.
    """
    nextEvaluationTime = valueFunction.nextEvaluationTime
    if nextEvaluationTime is None:
        return INFINITE
    return nextEvaluationTime
