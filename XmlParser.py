import re
from typing import Union, List, Dict
import numpy as np
from lxml import etree
from simpleeval import simple_eval
from sortedcontainers import SortedKeyList

from System import System
from Asset import Asset
from Transaction import Transaction
from TerminationCondition import TerminationCondition, AssetValueGreaterThanTerminationCondition, \
    AssetValueLessThanTerminationCondition
from Constants import *
from ValueFunction import SingleUseFixedValueFunction, SingleUsePercentageValueFunction, \
    RecurringPercentageValueFunction, RecurringFixedValueFunction, ValueFunction, SingleUseCalculatedValueFunction, \
    RecurringCalculatedValueFunction

# Object tags.
SYSTEM_TAG = "System"
ASSET_TAG = "Asset"
TRANSACTION_TAG = "Transaction"
VALUE_FUNCTION_TAG = "ValueFunction"
TERMINATION_CONDITION = "TerminationCondition"

# Attributes.
NAME_ATTRIB = "name"
EVAL_TIME_ATTRIB = "evaluationTime"
FIRST_EVAL_TIME_ATTRIB = "firstEvaluationTime"
EVAL_PERIOD_ATTRIB = "evaluationPeriod"
TERMINATION_TIME_ATTRIB = "terminationTime"
VALUE_ATTRIB = "value"
PERCENTAGE_ATTRIB = "percentage"
FORMULA_ATTRIB = "formula"
# Asset attributes.
INITIAL_VALUE_ATTRIB = "initialValue"
CREATION_TIME_ATTRIB = "creationTime"
# Transaction asset attributes.
FROM_ASSET_ATTRIB = "fromAsset"
TO_ASSET_ATTRIB = "toAsset"
# ValueFunction type attributes.
VALUE_FUNCTION_TYPE_ATTRIB = "type"
SINGLE_USE_FIXED_VALUE_FUNCTION_ATTRIB = "SingleUseFixedValueFunction"
SINGLE_USE_PERCENTAGE_VALUE_FUNCTION = "SingleUsePercentageValueFunction"
RECURRING_FIXED_VALUE_FUNCTION = "RecurringFixedValueFunction"
RECURRING_PERCENTAGE_VALUE_FUNCTION = "RecurringPercentageValueFunction"
FIXED_VALUE_FUNCTION_PROFILE = "FixedValueFunctionProfile"
PERCENTAGE_VALUE_FUNCTION_PROFILE = "PercentageValueFunctionProfile"
SINGLE_USE_CALCULATED_VALUE_FUNCTION = "SingleUseCalculatedValueFunction"
RECURRING_CALCULATED_VALUE_FUNCTION = "RecurringCalculatedValueFunction"
# Termination condition attributes.
ASSET_VALUE_GREATER_THAN_TERMINATION_CONDITION_ATTRIB = "AssetValueGreaterThan"
ASSET_VALUE_LESS_THAN_TERMINATION_CONDITION_ATTRIB = "AssetValueLessThan"
TERMINATION_CONDITION_TYPE_ATTRIB = "type"
TERMINATION_CONDITION_ASSET_ATTRIB = "asset"
TERMINATION_CONDITION_VALUE_ATTRIB = "value"


def getpath(element: etree.Element) -> str:
    """Gets the path to the given element."""
    return str(element.getroottree().getpath(element))


regex = re.compile(r'((?P<years>-?\d+?)y)?((?P<months>-?\d+?)m)?((?P<weeks>-?\d+?)w)?((?P<days>-?\d+?)d)?((?P<seconds>-?\d+?)s)?')
timeConversionDict = {"seconds": 1, "days": SECONDS_IN_DAY, "weeks": SECONDS_IN_WEEK, "months": SECONDS_IN_MONTH, "years": SECONDS_IN_YEAR}
def parseTime(timeStr: str) -> float:
    """Takes a string that is assumed to be of the format #(s) or #y#m#w#d#s and returns the number of seconds."""
    try:
        return float(timeStr)
    except:
        pass
    parts = regex.match(timeStr)
    if not parts:
        raise RuntimeError("The timestring {} could not be parsed using the format #y#m#w#d#s or #(s).".format(timeStr))
    return np.sum([timeConversionDict[name] * float(value) for (name, value) in parts.groupdict().items() if value])


def getDuplicateAssetNames(assets: SortedKeyList) -> List[str]:
    duplicates = []
    for i in range(len(assets)-1):
        for j in range(1, len(assets)-i):
            if assets[i].name == assets[i+j].name:
                duplicates.append(assets[i].name)
    return duplicates


class XmlParser:
    """A collection of static methods that read an Xml in and return various objects and data used for modelling."""

    @staticmethod
    def parseSystem(filepath: str) -> System:
        """A System is directly under the root node in an XML document."""
        # Get the XML tree.
        root: etree.Element = etree.parse(filepath).getroot()
        # Find the system node.
        for child in root:
            if child.tag == SYSTEM_TAG:
                # Process the system node.
                system = System(child.attrib[NAME_ATTRIB])
                # Parse all the assets first.
                for systemChild in child:
                    if systemChild.tag == ASSET_TAG:
                        system.addAsset(XmlParser.parseAsset(systemChild))
                # If we found duplicates raise an error.
                duplicates = getDuplicateAssetNames(system.assets)
                if duplicates:
                    raise RuntimeError("Duplicate asset names {}\nin file {}.".format(str(duplicates), filepath))
                # Parse all the transactions second so that the references to the assets can be passed in.
                for systemChild in child:
                    if systemChild.tag == TRANSACTION_TAG:
                        system.addTransaction(XmlParser.parseTransaction(systemChild, system.assets))
                    elif systemChild.tag == TERMINATION_CONDITION:
                        system.addTerminationCondition(XmlParser.parseTerminationCondition(systemChild, system.assets))
                    elif systemChild.tag == ASSET_TAG:
                        pass
                    else:
                        raise RuntimeError("Could not identify the systemChild tag type {}\nin tag {}\n"
                                           "from file {}".format(systemChild.tag, getpath(systemChild), filepath))
                return system
        raise RuntimeError("Could not find the system node in {}".format(filepath))

    @staticmethod
    def parseAsset(assetNode: etree.Element) -> Asset:
        # Create the asset that will be returned.
        kwargs: Dict = {}
        if INITIAL_VALUE_ATTRIB in assetNode.attrib:
            kwargs[INITIAL_VALUE_ATTRIB] = simple_eval(assetNode.attrib[INITIAL_VALUE_ATTRIB])
        if CREATION_TIME_ATTRIB in assetNode.attrib:
            kwargs[CREATION_TIME_ATTRIB] = parseTime(assetNode.attrib[CREATION_TIME_ATTRIB])
        asset = Asset(assetNode.attrib[NAME_ATTRIB], **kwargs)
        # Add the ValueFunctions to the asset.
        for assetChild in assetNode:
            if assetChild.tag == VALUE_FUNCTION_TAG:
                asset.addValueFunction(XmlParser.parseValueFunction(assetChild))
            else:
                raise RuntimeError("Could not identify the assetChild tag type {} in tag {}".format(
                    assetChild.tag, getpath(assetChild)))
        return asset

    @staticmethod
    def parseValueFunction(valueFunctionNode: etree.Element) -> ValueFunction:
        # Determine which type of ValueFunction we are returning.
        try:
            # Check for a single use fixed value function.
            if valueFunctionNode.attrib[VALUE_FUNCTION_TYPE_ATTRIB] == SINGLE_USE_FIXED_VALUE_FUNCTION_ATTRIB:
                return SingleUseFixedValueFunction(
                    valueFunctionNode.attrib[NAME_ATTRIB],
                    parseTime(valueFunctionNode.attrib[EVAL_TIME_ATTRIB]),
                    simple_eval(valueFunctionNode.attrib[VALUE_ATTRIB])
                )
            # Check for a single use percentage value function.
            elif valueFunctionNode.attrib[VALUE_FUNCTION_TYPE_ATTRIB] == SINGLE_USE_PERCENTAGE_VALUE_FUNCTION:
                return SingleUsePercentageValueFunction(
                    valueFunctionNode.attrib[NAME_ATTRIB],
                    parseTime(valueFunctionNode.attrib[EVAL_TIME_ATTRIB]),
                    simple_eval(valueFunctionNode.attrib[PERCENTAGE_ATTRIB])
                )
            # Check for a single use calculated value function.
            elif valueFunctionNode.attrib[VALUE_FUNCTION_TYPE_ATTRIB] == SINGLE_USE_CALCULATED_VALUE_FUNCTION:
                return SingleUseCalculatedValueFunction(
                    valueFunctionNode.attrib[NAME_ATTRIB],
                    parseTime(valueFunctionNode.attrib[EVAL_TIME_ATTRIB]),
                    valueFunctionNode.attrib[FORMULA_ATTRIB]
                )
            # Check for a recurring fixed value function.
            elif valueFunctionNode.attrib[VALUE_FUNCTION_TYPE_ATTRIB] == RECURRING_FIXED_VALUE_FUNCTION:
                # Check whether there is a termination time.
                if TERMINATION_TIME_ATTRIB in valueFunctionNode.attrib:
                    terminationTime = parseTime(valueFunctionNode.attrib[TERMINATION_TIME_ATTRIB])
                else:
                    terminationTime = float("inf")
                # Create the ValueFunction.
                return RecurringFixedValueFunction(
                    valueFunctionNode.attrib[NAME_ATTRIB],
                    parseTime(valueFunctionNode.attrib[FIRST_EVAL_TIME_ATTRIB]),
                    parseTime(valueFunctionNode.attrib[EVAL_PERIOD_ATTRIB]),
                    simple_eval(valueFunctionNode.attrib[VALUE_ATTRIB]),
                    terminationTime
                )
            # Check for a recurring percentage value function.
            elif valueFunctionNode.attrib[VALUE_FUNCTION_TYPE_ATTRIB] == RECURRING_PERCENTAGE_VALUE_FUNCTION:
                # Check whether there is a termination time.
                if TERMINATION_TIME_ATTRIB in valueFunctionNode.attrib:
                    terminationTime = parseTime(valueFunctionNode.attrib[TERMINATION_TIME_ATTRIB])
                else:
                    terminationTime = float("inf")
                # Create the ValueFunction.
                return RecurringPercentageValueFunction(
                    valueFunctionNode.attrib[NAME_ATTRIB],
                    parseTime(valueFunctionNode.attrib[FIRST_EVAL_TIME_ATTRIB]),
                    parseTime(valueFunctionNode.attrib[EVAL_PERIOD_ATTRIB]),
                    simple_eval(valueFunctionNode.attrib[PERCENTAGE_ATTRIB]),
                    terminationTime
                )
            # Check for a recurring fixed value function.
            elif valueFunctionNode.attrib[VALUE_FUNCTION_TYPE_ATTRIB] == RECURRING_CALCULATED_VALUE_FUNCTION:
                # Check whether there is a termination time.
                if TERMINATION_TIME_ATTRIB in valueFunctionNode.attrib:
                    terminationTime = parseTime(valueFunctionNode.attrib[TERMINATION_TIME_ATTRIB])
                else:
                    terminationTime = float("inf")
                # Create the ValueFunction.
                return RecurringCalculatedValueFunction(
                    valueFunctionNode.attrib[NAME_ATTRIB],
                    parseTime(valueFunctionNode.attrib[FIRST_EVAL_TIME_ATTRIB]),
                    parseTime(valueFunctionNode.attrib[EVAL_PERIOD_ATTRIB]),
                    valueFunctionNode.attrib[FORMULA_ATTRIB],
                    terminationTime
                )
            # Check for any unhandled cases.
            else:
                raise RuntimeError("ValueFunction of type {} has not been implemented.\n"
                                   "Attempted to use it in tag {}.".format(
                    valueFunctionNode.attrib[VALUE_FUNCTION_TYPE_ATTRIB], getpath(valueFunctionNode)))
        # Catch any exceptions thrown from the attempted parsing.
        except Exception as e:
            raise RuntimeError("Exception raised while parsing ValueFunction with message: {}\n"
                               "Exception occurred in tag {}.".format(str(e), getpath(valueFunctionNode)))

    @staticmethod
    def parseTransaction(transactionNode: etree.Element, assets: SortedKeyList) -> Transaction:

        # Search through the children of transactionNode for the valueFunctionNode.
        for child in transactionNode:
            if child.tag == VALUE_FUNCTION_TAG:
                # Get the fromAsset. Raise an error if it doesn't exist.
                fromAsset = XmlParser.getAsset(assets, transactionNode.attrib[FROM_ASSET_ATTRIB])
                if fromAsset is None:
                    raise RuntimeError("fromAsset name {} was not a recognised asset name in the system.".format(
                        transactionNode.attrib[FROM_ASSET_ATTRIB]))
                # Get the toAsset. Raise an error if it doesn't exist.
                toAsset = XmlParser.getAsset(assets, transactionNode.attrib[TO_ASSET_ATTRIB])
                if toAsset is None:
                    raise RuntimeError("toAsset name {} was not a recognised asset name in the system.".format(
                        transactionNode.attrib[TO_ASSET_ATTRIB]))
                # Create the transaction that will be returned.
                return Transaction(
                    transactionNode.attrib[NAME_ATTRIB], XmlParser.parseValueFunction(child), fromAsset, toAsset)
            else:
                raise RuntimeError("A child of a Transaction node was not a ValueFunction node. Check {}".format(getpath(child)))

    @staticmethod
    def parseTerminationCondition(terminationConditionNode: etree.Element, assets: SortedKeyList) -> TerminationCondition:
        # Determine which type of TerminationCondition we are returning.
        try:
            # Check for "asset value greater than" termination condition.
            if terminationConditionNode.attrib[TERMINATION_CONDITION_TYPE_ATTRIB] == ASSET_VALUE_GREATER_THAN_TERMINATION_CONDITION_ATTRIB:
                # Get the asset. Raise an error if it doesn't exist.
                assetName = terminationConditionNode.attrib[TERMINATION_CONDITION_ASSET_ATTRIB]
                asset = XmlParser.getAsset(assets, assetName)
                if asset is None:
                    raise RuntimeError(f"asset name {assetName} was not a recognised asset name in the system.")
                # Create the termination condition to be returned.
                return AssetValueGreaterThanTerminationCondition(
                    asset,
                    simple_eval(terminationConditionNode.attrib[TERMINATION_CONDITION_VALUE_ATTRIB])
                )
            # Check for "asset value less than" termination condition.
            elif terminationConditionNode.attrib[TERMINATION_CONDITION_TYPE_ATTRIB] == ASSET_VALUE_LESS_THAN_TERMINATION_CONDITION_ATTRIB:
                # Get the asset. Raise an error if it doesn't exist.
                assetName = terminationConditionNode.attrib[TERMINATION_CONDITION_ASSET_ATTRIB]
                asset = XmlParser.getAsset(assets, assetName)
                if asset is None:
                    raise RuntimeError(f"asset name {assetName} was not a recognised asset name in the system.")
                # Create the termination condition to be returned.
                return AssetValueLessThanTerminationCondition(
                    asset,
                    simple_eval(terminationConditionNode.attrib[TERMINATION_CONDITION_VALUE_ATTRIB])
                )
            # Check for any unhandled cases.
            else:
                raise RuntimeError(
                    f"TerminationCondition of type {terminationConditionNode.attrib[TERMINATION_CONDITION_TYPE_ATTRIB]} has not been implemented.\n"
                    f"Attempted to use it in tag {getpath(terminationConditionNode)}"
                )
        except Exception as e:
            raise RuntimeError(f"Exception raised while parsing TerminationCondition with message: {str(e)}\n"
                               f"Exception occurred in tag {getpath(terminationConditionNode)}.")

    @staticmethod
    def getAsset(assets: SortedKeyList, assetName: str) -> Union[Asset, None]:
        for asset in assets:
            if asset.name == assetName:
                return asset
        return None
