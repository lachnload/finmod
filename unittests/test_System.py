import os
import unittest

from Constants import *
from XmlParser import XmlParser

# Test filenames.
TEST_FIXED_VALUE_FUNCTIONS = "TestFixedValueFunctions.xml"
TEST_PERCENTAGE_VALUE_FUNCTIONS = "TestPercentageValueFunctions.xml"
TEST_TRANSACTIONS = "TestTransactions.xml"
TEST_ASSET_VALUE_GREATER_THAN_TERMINATION_CONDITION = "TestTerminationConditionsAssetValueGreaterThan.xml"
TEST_ASSET_VALUE_LESS_THAN_TERMINATION_CONDITION = "TestTerminationConditionsAssetValueLessThan.xml"
TEST_CALCULATED_VALUE_FUNCTIONS = "TestCalculatedValueFunctions.xml"


class SystemTests(unittest.TestCase):

    def test_simulateSystem(self):
        """Check that a system correctly simulates."""
        # Load the system for the fixed value test.
        system = XmlParser.parseSystem(os.path.join(os.getcwd(), UNIT_TEST_FOLDERNAME, TEST_FIXED_VALUE_FUNCTIONS))
        # Simulate the system.
        system.simulate(INFINITE)
        # Check the simulation final result.
        self.assertEqual(system.assets[0].value, 22500)
        # Check the logging captured the asset value changes.
        asset0 = system.assets[0]
        self.assertEqual(asset0.log[-1 * SECONDS_IN_WEEK], 10000)
        self.assertEqual(asset0.log[0 * SECONDS_IN_WEEK], 20000)
        self.assertEqual(asset0.log[1 * SECONDS_IN_WEEK], 19500)
        self.assertEqual(asset0.log[2 * SECONDS_IN_WEEK], 21500)
        self.assertEqual(asset0.log[3 * SECONDS_IN_WEEK], 21000)
        self.assertEqual(asset0.log[4 * SECONDS_IN_WEEK], 23000)
        self.assertEqual(asset0.log[5 * SECONDS_IN_WEEK], 22500)

        # Load the system for the percentage value test.
        system = XmlParser.parseSystem(os.path.join(os.getcwd(), UNIT_TEST_FOLDERNAME, TEST_PERCENTAGE_VALUE_FUNCTIONS))
        # Simulate the system.
        system.simulate(INFINITE)
        # Check the simulation final result.
        self.assertEqual(system.assets[0].value, 33750)
        # Check the logging captured the asset value changes.
        asset0 = system.assets[0]
        self.assertEqual(asset0.log[0 * SECONDS_IN_DAY], 10000)
        self.assertEqual(asset0.log[1 * SECONDS_IN_DAY], 20000)
        self.assertEqual(asset0.log[2 * SECONDS_IN_WEEK], 30000)
        self.assertEqual(asset0.log[4 * SECONDS_IN_WEEK], 45000)
        self.assertEqual(asset0.log[5 * SECONDS_IN_WEEK], 33750)

        # Load the system for the transaction test.
        system = XmlParser.parseSystem(os.path.join(os.getcwd(), UNIT_TEST_FOLDERNAME, TEST_TRANSACTIONS))
        # Simulate the system.
        system.simulate(4 * SECONDS_IN_WEEK)
        # Check the simulation final result.
        for asset in system.assets:
            if asset.name == "Bank":
                self.assertEqual(asset.value, 12000)
            if asset.name == "Loan":
                self.assertEqual(asset.value, -2000)
        # Check the logging captured the asset value changes.
        asset0 = system.assets[0]
        asset1 = system.assets[1]
        self.assertEqual(asset0.log[0 * SECONDS_IN_DAY], 10000)
        self.assertEqual(asset1.log[0 * SECONDS_IN_DAY], 0)
        self.assertEqual(asset0.log[1 * SECONDS_IN_DAY], 16000)
        self.assertEqual(asset1.log[1 * SECONDS_IN_DAY], -6000)
        self.assertEqual(asset0.log[1 * SECONDS_IN_WEEK], 15000)
        self.assertEqual(asset1.log[1 * SECONDS_IN_WEEK], -5000)
        self.assertEqual(asset0.log[2 * SECONDS_IN_WEEK], 14000)
        self.assertEqual(asset1.log[2 * SECONDS_IN_WEEK], -4000)
        self.assertEqual(asset0.log[3 * SECONDS_IN_WEEK], 13000)
        self.assertEqual(asset1.log[3 * SECONDS_IN_WEEK], -3000)
        self.assertEqual(asset0.log[4 * SECONDS_IN_WEEK], 12000)
        self.assertEqual(asset1.log[4 * SECONDS_IN_WEEK], -2000)

        # Load the system for the "greater than" termination condition test.
        system = XmlParser.parseSystem(os.path.join(
            os.getcwd(),
            UNIT_TEST_FOLDERNAME,
            TEST_ASSET_VALUE_GREATER_THAN_TERMINATION_CONDITION)
        )
        # Simulate the system.
        system.simulate(4 * SECONDS_IN_WEEK)
        # Check the simulation end time.
        self.assertEqual(system.lastEvaluationTime, 0)
        # Check the simulation final result.
        for asset in system.assets:
            if asset.name == "Bank":
                self.assertEqual(asset.value, 20000)

        # Load the system for the "lesser than" termination condition test.
        system = XmlParser.parseSystem(os.path.join(
            os.getcwd(),
            UNIT_TEST_FOLDERNAME,
            TEST_ASSET_VALUE_LESS_THAN_TERMINATION_CONDITION)
        )
        # Simulate the system.
        system.simulate(4 * SECONDS_IN_WEEK)
        # Check the simulation end time.
        self.assertEqual(system.lastEvaluationTime, 0)
        # Check the simulation final result.
        for asset in system.assets:
            if asset.name == "Bank":
                self.assertEqual(asset.value, 0)

        # Load the system for the calculated function test.
        system = XmlParser.parseSystem(os.path.join(
            os.getcwd(),
            UNIT_TEST_FOLDERNAME,
            TEST_CALCULATED_VALUE_FUNCTIONS
        ))
        # Simulate the system.
        system.simulate(2 * SECONDS_IN_WEEK)
        # Check the simulation final result.
        for asset in system.assets:
            if asset.name == "Asset1":
                self.assertEqual(asset.value, 1000)
            if asset.name == "Asset2":
                self.assertEqual(asset.value, 9190)
