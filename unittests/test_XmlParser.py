import os
import unittest

from ValueFunction import SingleUseFixedValueFunction, RecurringFixedValueFunction, RecurringPercentageValueFunction
from XmlParser import XmlParser, parseTime
from Constants import *

TEST_MODEL_FILENAME = "TestModel.xml"


class XmlParserTests(unittest.TestCase):

    def test_parseSystem(self):
        """Check that a System is correctly parsed."""
        system = XmlParser.parseSystem(os.path.join(os.getcwd(), UNIT_TEST_FOLDERNAME, TEST_MODEL_FILENAME))
        self.assertEqual(system.name, "TestModel")
        asset0 = system.assets[0]
        self.assertEqual(asset0.name, "SelfLiquidity")
        self.assertEqual(asset0.lastEvaluationTime, 0.0)
        self.assertEqual(asset0.value, 0.0)
        asset0valueFunction0: SingleUseFixedValueFunction = asset0.valueFunctions[0]
        self.assertEqual(asset0valueFunction0.name, "InitialSaving")
        self.assertEqual(type(asset0valueFunction0), SingleUseFixedValueFunction)
        self.assertEqual(asset0valueFunction0.evaluated, False)
        self.assertEqual(asset0valueFunction0.value, 10000)
        self.assertEqual(asset0valueFunction0.nextEvaluationTime, 0.0)
        asset0valueFunction1: RecurringFixedValueFunction = asset0.valueFunctions[1]
        self.assertEqual(asset0valueFunction1.name, "Rent")
        self.assertEqual(type(asset0valueFunction1), RecurringFixedValueFunction)
        self.assertEqual(asset0valueFunction1.value, -250)
        self.assertEqual(asset0valueFunction1.evaluationPeriod, SECONDS_IN_WEEK)
        self.assertEqual(asset0valueFunction1.nextEvaluationTime, SECONDS_IN_WEEK)
        self.assertEqual(asset0valueFunction1.terminationTime, float("inf"))
        asset0valueFunction2: RecurringFixedValueFunction = asset0.valueFunctions[2]
        self.assertEqual(asset0valueFunction2.name, "RocketLabIncome")
        self.assertEqual(type(asset0valueFunction2), RecurringFixedValueFunction)
        self.assertEqual(asset0valueFunction2.value, 2500)
        self.assertEqual(asset0valueFunction2.evaluationPeriod, 2 * SECONDS_IN_WEEK)
        self.assertEqual(asset0valueFunction2.nextEvaluationTime, 2 * SECONDS_IN_WEEK)
        self.assertEqual(asset0valueFunction2.terminationTime, float("inf"))
        asset1 = system.assets[1]
        self.assertEqual(asset1.name, "Mortgage")
        self.assertEqual(asset1.lastEvaluationTime, 0.0)
        self.assertEqual(asset1.value, -700000)
        asset1valueFunction0: SingleUseFixedValueFunction = asset1.valueFunctions[0]
        self.assertEqual(asset1valueFunction0.name, "InitialMortgage")
        self.assertEqual(type(asset1valueFunction0), SingleUseFixedValueFunction)
        self.assertEqual(asset1valueFunction0.evaluated, False)
        self.assertEqual(asset1valueFunction0.value, -700000)
        self.assertEqual(asset1valueFunction0.nextEvaluationTime, 0.0)
        asset1valueFunction1: RecurringPercentageValueFunction = asset1.valueFunctions[1]
        self.assertEqual(asset1valueFunction1.name, "CompoundingInterest")
        self.assertEqual(type(asset1valueFunction1), RecurringPercentageValueFunction)
        self.assertEqual(asset1valueFunction1.percentage, 0.04/12)
        self.assertEqual(asset1valueFunction1.evaluationPeriod, SECONDS_IN_MONTH)
        self.assertEqual(asset1valueFunction1.nextEvaluationTime, SECONDS_IN_MONTH)
        self.assertEqual(asset1valueFunction1.terminationTime, float("inf"))
        asset2 = system.assets[2]
        self.assertEqual(asset2.name, "Property")
        self.assertEqual(asset2.lastEvaluationTime, -SECONDS_IN_WEEK)
        self.assertEqual(asset2.value, 700000)
        asset2valueFunction0: SingleUseFixedValueFunction = asset2.valueFunctions[0]
        self.assertEqual(asset2valueFunction0.name, "InitialPropertyValue")
        self.assertEqual(type(asset2valueFunction0), SingleUseFixedValueFunction)
        self.assertEqual(asset2valueFunction0.evaluated, False)
        self.assertEqual(asset2valueFunction0.value, 700000)
        self.assertEqual(asset2valueFunction0.nextEvaluationTime, 0.0)
        asset2valueFunction1: RecurringPercentageValueFunction = asset2.valueFunctions[1]
        self.assertEqual(asset2valueFunction1.name, "PropertyAppreciation")
        self.assertEqual(type(asset2valueFunction1), RecurringPercentageValueFunction)
        self.assertEqual(asset2valueFunction1.percentage, 0.05/12)
        self.assertEqual(asset2valueFunction1.evaluationPeriod, SECONDS_IN_MONTH)
        self.assertEqual(asset2valueFunction1.nextEvaluationTime, SECONDS_IN_MONTH)
        self.assertEqual(asset2valueFunction1.terminationTime, 12 * SECONDS_IN_YEAR)
        asset2valueFunction2: RecurringFixedValueFunction = asset2.valueFunctions[2]
        self.assertEqual(asset2valueFunction2.name, "Maintenance")
        self.assertEqual(type(asset2valueFunction2), RecurringFixedValueFunction)
        self.assertEqual(asset2valueFunction2.value, -12000)
        self.assertEqual(asset2valueFunction2.evaluationPeriod, SECONDS_IN_YEAR)
        self.assertEqual(asset2valueFunction2.nextEvaluationTime, SECONDS_IN_YEAR)
        self.assertEqual(asset2valueFunction2.terminationTime, float("inf"))

    def test_parseTime(self):

        time = "12345"
        self.assertEqual(parseTime(time), 12345)

        time = r"1d"
        self.assertEqual(parseTime(time), SECONDS_IN_DAY)
        time = r"1w"
        self.assertEqual(parseTime(time), SECONDS_IN_WEEK)
        time = r"1m"
        self.assertEqual(parseTime(time), SECONDS_IN_MONTH)
        time = r"1y"
        self.assertEqual(parseTime(time), SECONDS_IN_YEAR)
        time = r"1y1w1d"
        self.assertEqual(parseTime(time), SECONDS_IN_YEAR + SECONDS_IN_WEEK + SECONDS_IN_DAY)
