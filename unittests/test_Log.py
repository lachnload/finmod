import unittest
from collections import OrderedDict

from Log import Log, sumLogs


class LogTests(unittest.TestCase):

    def test_sumLogs(self):
        """Check that summing logs together works as expected."""
        # Build some logs.
        log1 = Log({-1: 10, 10: 10, 2: 5, 20: 10})
        log2 = Log({0: 0, 1: -10, 30: 0})
        # Add the logs together.
        log3 = sumLogs(log1, log2)
        # Check the summed log.
        self.assertDictEqual(log3.dataDict, OrderedDict([(-1, 10), (0, 10), (1, 0), (2, -5), (10, 0), (20, 0), (30, 10)]))
