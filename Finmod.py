import argparse
import os

from Constants import INFINITE
from System import System, saveSystemSimulationLogs
from XmlParser import XmlParser, parseTime
from analysis.LogPlotter import runLogPlotter


def runFinmod(args):
    """Loads the system model, runs a simulation of it and saves the logs."""
    # Load the system model.
    system: System = XmlParser.parseSystem(args.modelFilepath)
    # Simulate the system.
    system.simulate(parseTime(args.simulationTerminationTime))
    # Save the logs.
    # Save the logs to a designated system folder.
    folderpath = os.path.join(args.outputFolderpath, system.name)
    saveSystemSimulationLogs(folderpath, system)
    # Maybe do some plotting.
    if args.interactivePlotting:
        runLogPlotter(folderpath)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--modelFilepath", help="The filepath to the system model that we'll be simulating.",
                        required=True, type=str)
    parser.add_argument("--outputFolderpath", help="The folderpath that we'll output the logs and plots to.",
                        required=True, type=str)
    parser.add_argument("--simulationTerminationTime", help="The time to terminate the simulation.",
                        required=False, type=str, default=INFINITE)
    parser.add_argument("--interactivePlotting", help="Indicates whether we enter the interactive plotting for analysis.",
                        required=False, type=bool)
    args = parser.parse_args()

    runFinmod(args)
