from ValueFunction import *
from Asset import Asset


class Transaction:
    """
    A transaction is a ValueFunction that is transferred from an Asset to another Asset.
    """

    def __init__(self, name: str, valueFunction: ValueFunction, fromAsset: Asset, toAsset: Asset):
        self.name = name
        self.valueFunction: ValueFunction = valueFunction
        self.fromAsset: Asset = fromAsset
        self.toAsset: Asset = toAsset
        self.lastEvaluationTime = -float("inf")
        self.lastTransactionValue = 0.0
        self.log: Dict[float, float] = {}

    def getNextEvaluationTime(self):
        return self.valueFunction.nextEvaluationTime

    def getLog(self) -> Log:
        return Log(self.log)

    def update(self, assetsState: AssetsState):
        """Applies valueFunction to fromAsset and applies the negated valueFunction to the toAsset."""

        def updateAndGetValueFunction(self: Transaction):
            """Helper function for updating and applying value functions."""
            # Determine whether the value function is fixed or percentage.
            if issubclass(type(self.valueFunction), FixedValueFunction):
                fixedValueFunction: FixedValueFunction = self.valueFunction
                return fixedValueFunction.getValueAndUpdateNextEvaluationTime()
            elif issubclass(type(self.valueFunction), PercentageValueFunction):
                percentageValueFunction: PercentageValueFunction = self.valueFunction
                return percentageValueFunction.getValueAndUpdateNextEvaluationTime(self.fromAsset.value)
            elif issubclass(type(self.valueFunction), CalculatedValueFunction):
                calculatedValueFunction: CalculatedValueFunction = self.valueFunction
                return calculatedValueFunction.getValueAndUpdateNextEvaluationTime(assetsState)
            else:
                raise RuntimeWarning("Applying ValueFunction of type {} has not been implemented in {}".format(str(type(self.valueFunction)), __name__))

        # Update the fromAsset and toAsset with the transactionValue. The toAsset gets the transactionValue taken off
        # the fromAsset.
        self.lastEvaluationTime = self.getNextEvaluationTime()
        self.lastTransactionValue = updateAndGetValueFunction(self)
        self.fromAsset.transactionUpdate(-self.lastTransactionValue, self.lastEvaluationTime)
        self.toAsset.transactionUpdate(self.lastTransactionValue, self.lastEvaluationTime)
        # Log the transaction. This will be logged as the value change of the toAsset.
        self.log[self.lastEvaluationTime] = self.lastTransactionValue


def getTransactionNextEvaluationTime(transaction: Transaction) -> float:
    """
    Takes a Transaction and returns the next time that this Transaction is supposed to be evaluated. If it isn't
    supposed to be evaluated it returns infinite.

    :param transaction: The Transaction to get the time from.
    :return: A float representing the next time in seconds that the Transaction is supposed to be evaluated.
    """
    nextEvaluationTime = transaction.getNextEvaluationTime()
    if nextEvaluationTime is None:
        return float("inf")
    return nextEvaluationTime
